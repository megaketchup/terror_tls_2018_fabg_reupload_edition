﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanityController : MonoBehaviour {
	public float spooklevel=0;
	public float peak_spooklevel=100;
	bool is_continually_spook = false;
	float continued_spook_factor=0;
	public float recover_mult = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (spooklevel >= peak_spooklevel) {
			is_continually_spook = false;
		//gameover
		}
		if (spooklevel < 0) {
			spooklevel = 0;
		}
		if (is_continually_spook == true) {
			spooklevel += (continued_spook_factor * Time.deltaTime);
		} else {
			spooklevel -= (Time.deltaTime*recover_mult);
		}
		
	}

	public void SpookPlayer(float spookpoints) {
		spooklevel = spooklevel + spookpoints;
		

	}
	public void ContinuallySpookPlayer(float spookfactor) {
		is_continually_spook = true;
		continued_spook_factor = spookfactor;


	}
	public void StopSpookingPlayer() {
		is_continually_spook = false;



	}
}
