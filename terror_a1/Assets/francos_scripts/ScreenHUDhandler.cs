﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets.Characters.FirstPerson{
	public class ScreenHUDhandler : MonoBehaviour {
		string localgamestate="gameworld";
		public string hud_element ="none";
		Vector3 og_scale=new Vector3(0,0,0);
		// Use this for initialization
		void Start () {
			og_scale = transform.localScale;

		}

		// Update is called once per frame
		void Update () {
			localgamestate = GameObject.Find("Player").GetComponent<input_handler>().gamestate;
			switch (localgamestate) {
			case "pause":
				DrawPauseHUD ();
				break;
			case"gameworld":
				DrawGameWorldHUD ();
				break;
			case "text":
				DrawTextHUD ();
				break;
			case "input":
				DrawInputHUD ();
				break;
			case "safe":
				DrawSafeHUD ();
				break;


			}

		}

		void DrawGameWorldHUD ()
		{
			transform.localScale=new Vector3(0,0,0);
			Cursor.visible = false;

			//GetComponentInChildren<CanvasRenderer> ().transform.localScale=new Vector3(0,0,0) ;

		}
		void DrawPauseHUD ()
		{
			if (hud_element == "pause") {
				//GetComponentInChildren<CanvasRenderer> ().SetAlpha (1);
				transform.localScale = og_scale;
				//	GetComponentInChildren<CanvasRenderer> ().transform.localScale=new Vector3(0.8f,0.8f,1) ;

			} else {
				transform.localScale=new Vector3(0,0,0);

			}
			Cursor.visible = true;
		}

		void DrawTextHUD () 
		{
			if (hud_element == "text") 
			{
				//GetComponentInChildren<CanvasRenderer> ().SetAlpha (1);
				transform.localScale = og_scale;
				Debug.Log ("text visible");


				//	GetComponentInChildren<CanvasRenderer> ().transform.localScale=new Vector3(0.8f,0.8f,1) ;

			}
			else {
				transform.localScale=new Vector3(0,0,0);

			}
			Cursor.visible = true;
		}

		void DrawInputHUD ()
		{
			if (hud_element == "input") {
				//GetComponentInChildren<CanvasRenderer> ().SetAlpha (1);
				transform.localScale = og_scale;
				//	GetComponentInChildren<CanvasRenderer> ().transform.localScale=new Vector3(0.8f,0.8f,1) ;

			} else {
				transform.localScale=new Vector3(0,0,0);

			}

		}
		void DrawSafeHUD ()
		{
			if (hud_element == "safe") {
				//GetComponentInChildren<CanvasRenderer> ().SetAlpha (1);
				transform.localScale = og_scale;
				//	GetComponentInChildren<CanvasRenderer> ().transform.localScale=new Vector3(0.8f,0.8f,1) ;

			} else {
				transform.localScale=new Vector3(0,0,0);

			}

		}

	}
}