﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpookyDarknessController : MonoBehaviour {
	public float mult=1;
	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter (Collider col) {
		if (col.gameObject.tag == "Player") {
			col.gameObject.GetComponent<SanityController>().ContinuallySpookPlayer(mult);
		}
	}
	void OnTriggerExit (Collider col) {
		if (col.gameObject.tag == "Player") {
			col.gameObject.GetComponent<SanityController>().StopSpookingPlayer();
		}
	}

}
