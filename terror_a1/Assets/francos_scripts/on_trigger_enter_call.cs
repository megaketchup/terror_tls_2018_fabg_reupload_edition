﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets.Characters.FirstPerson
{
	public class on_trigger_enter_call : MonoBehaviour 
	{
		string current_gamestate="gameworld";
		int timer_max=6;
		int timer_curr=0;
		// Use this for initialization
		void Start () {

		}

		// Update is called once per frame
		void Update () {

			current_gamestate = GameObject.Find ("Player").GetComponent<input_handler> ().gamestate;
		}

		void OnTriggerEnter () {

			Invoke ("TriggerAction",0.0f);


		}
		void OnTriggerExit () {
			GameObject.Find("Player").GetComponent<input_handler>().gamestate="gameworld";
		}
		void TriggerAction ()
		{
			//if (current_gamestate != "pause") {
			GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "input";
			//}
		}
	}}

