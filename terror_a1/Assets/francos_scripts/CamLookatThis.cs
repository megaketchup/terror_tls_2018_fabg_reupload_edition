﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Characters.FirstPerson
{

public class CamLookatThis : MonoBehaviour 
{

		string localgamestate="none";
	public Transform camTarget;

	bool estaMirandoObjeto;
		float timer=0;
		bool timer_on=false;

	void Start()
	{
		estaMirandoObjeto = false;
	}

	
	// Update is called once per frame
	void Update () 
	{
			
			if (timer_on == true) {
				
				if (timer < 10) {
					timer += 1;
				}
				if (timer >= 10) {
					timer = 0;
					timer_on = false;
				}
			}
		RaycastHit hit;
		int lookableLayer = 1 << LayerMask.NameToLayer ("lookableobject");

		if (Physics.Raycast (camTarget.position, camTarget.forward, out hit, 2, lookableLayer)) 
		{
			Debug.Log (hit.collider.gameObject.name);
				if (estaMirandoObjeto == false) {
					GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "input";
					string text = hit.collider.gameObject.GetComponent<LookableObject> ().text;
					Debug.Log (text);
					GameObject.Find ("player_action_text").GetComponent<Text> ().text = text;
				}
			estaMirandoObjeto = true;

		} 
		else 
		{
				localgamestate = GameObject.Find ("Player").GetComponent<input_handler> ().gamestate;
				if (estaMirandoObjeto == true) {
					estaMirandoObjeto = false;
					timer_on = true;
					Debug.Log ("DEJO DE MIRAR OBJETO");
					if (timer >= 5&&localgamestate=="input") {
						GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "gameworld";
					}
				} else {
					if (timer >= 5&&localgamestate=="input") {
					GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "gameworld";
				}
				}
		}
	}
}
}
