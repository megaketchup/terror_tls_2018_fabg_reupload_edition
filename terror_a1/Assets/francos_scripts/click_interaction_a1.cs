﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Characters.FirstPerson
{

public class click_interaction_a1 : MonoBehaviour {
		public bool does_it_dissappear =false;


	public string objecttype ="none";
		Vector3 og_box_scale=new Vector3(0,0,0);

	// Use this for initialization
	void Start () {
			//og_box_scale=GameObject.Find("Player").GetComponent<BoxCollider>().transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnMouseDown()
	{

			//GameObject.Find("Player").GetComponentInChildren<BoxCollider>().transform.localScale = new Vector3 (0, 0, 0);
			Debug.Log ("calling down");
			if(objecttype=="text1")
			{
				transform.position = transform.position + new Vector3 (4, 4, 4);
				GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "text";
				GameObject.Find ("Player").GetComponent<FirstPersonController> ().m_MouseLook.SetCursorLock (false);

				GameObject.Find ("actual_text").GetComponent<text_handler> ().text_index=1;
				GameObject.Find ("actual_text").GetComponent<text_handler> ().SetText();
			}
			if(objecttype=="text2")
			{
				transform.position = transform.position + new Vector3 (4, 4, 4);
				GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "text";
				GameObject.Find ("Player").GetComponent<FirstPersonController> ().m_MouseLook.SetCursorLock (false);

				GameObject.Find ("actual_text").GetComponent<text_handler> ().text_index=2;
				GameObject.Find ("actual_text").GetComponent<text_handler> ().SetText();
			}
			if(objecttype=="text3")
			{
				transform.position = transform.position + new Vector3 (4, 4, 4);
				GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "text";
				GameObject.Find ("Player").GetComponent<FirstPersonController> ().m_MouseLook.SetCursorLock (false);

				GameObject.Find ("actual_text").GetComponent<text_handler> ().text_index=3;
				GameObject.Find ("actual_text").GetComponent<text_handler> ().SetText();
			}
			if(objecttype=="text4")
			{
				transform.position = transform.position + new Vector3 (4, 4, 4);
				GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "text";
				GameObject.Find ("Player").GetComponent<FirstPersonController> ().m_MouseLook.SetCursorLock (false);

				GameObject.Find ("actual_text").GetComponent<text_handler> ().text_index=4;
				GameObject.Find ("actual_text").GetComponent<text_handler> ().SetText();
			}
			if(objecttype=="safe")
			{
				Debug.Log ("safe enabled");
				//transform.position = transform.position + new Vector3 (4, 4, 4);
				GameObject.Find ("Player").GetComponent<input_handler> ().gamestate = "safe";
				GameObject.Find ("Player").GetComponent<FirstPersonController> ().m_MouseLook.SetCursorLock (false);
			}
			if (does_it_dissappear) {
				Destroy( this.gameObject.GetComponent<MeshRenderer> ());
			}

	}
//		void OnMouseUp()
//		{
//
//			GameObject.Find("Player").GetComponent<BoxCollider>().transform.localScale = og_box_scale;
//		
//
//
//		}
}
}
